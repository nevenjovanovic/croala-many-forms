# CroALa: one collection, many forms

[Neven Jovanović](orcid.org/0000-0002-9119-399X), Department of Classical Philology, Faculty of Humanities and Social Sciences, University of Zagreb

![Baška, Croatia, June 2017](img/bs_clouds.jpg)

## What?

A presentation for the [2nd International Symposium Digital Humanities: Empowering Visibility of Croatian Cultural Heritage](http://conference.unizd.hr/isdh/), November 6-8, 2017, Zadar.

# Summary

The digital collection Croatiae auctores Latini (CroALa [1]) publishes
Latin texts written by authors connected with Croatia, from the 10 th
to the 20 th century. The collection was first published in 2009. At
the moment it comprises some 5 million words and over 450 documents,
as well as an additional digital bibliography of Croatian Latin
(CroALaBib) and an experimental annotation corpus CroALa index
locorum. The collection and all its additional resources are published
under a CC Open License; they have been made available in several
forms and formats – as a searchable database (the search engine is
PhiloLogic, in its versions 3 and 4 [2]), and as a version-controlled
repository of XML documents and other files, published on Zenodo and
Github [3]. The open license and standardised formats made it possible
for the CroALa texts to be included in two larger corpora of Latin:
Corpus corporum (University of Zürich, [4]) and the Perseus Digital
Library [5]. Moreover, the texts have been excerpted for the digital
lexicon Neulateinische Wortliste [6] and used in a large-scale
comparison with other neo-Latin corpora [7]. The paper describes the
standards we have followed – what had to be added and documented to
enable interchange of our texts – and the enhancement of our
collection with texts published elsewhere under similar conditions
(open licenses, linked open data protocols), for example in the
Wolfenbütteler Digitale Bibliothek [8], in CAMENA - Lateinische Texte
der Frühen Neuzeit [9], and in the Poeti d' Italia in lingua latina
collection [10].


## How?

The slides for the presentation are made with the [reveal.js](https://github.com/hakimel/reveal.js/) HTML presentation framework.

# Licence

[CC-BY](LICENSE.md)
